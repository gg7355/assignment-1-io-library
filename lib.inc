section .text

%define SYS_WRITE 1
%define FD_STDOUT 1
%define FD_STDERR 2
%define FD_STDIN 0
%define SYS_READ 0
%define NEW_LINE 0xA
%define SPACE 0x20
%define TAB 0x9
%define MINUS '-'
%define PLUS '+'
%define ZERO_DIGIT '0'
%define NINE_DIGIT '9'

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor  rax, rax
.loop:
    cmp  byte [rdi+rax], 0
    je   .end 
    inc  rax
    jmp .loop 
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax 
    mov  rax, SYS_WRITE
    mov  rdi, FD_STDOUT
    syscall
    ret                

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_WRITE
    mov rdi, FD_STDOUT
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:   
    mov rsi, rsp
    mov r8, 10
    sub rsp, 32
    dec rsi
    mov byte [rsi], 0  
    mov rax, rdi
.loop:
    xor rdx, rdx
    div r8
    add rdx, ZERO_DIGIT
    dec rsi
    mov byte[rsi], dl
    test rax, rax
    jne .loop

    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .print
    push rdi
    mov rdi, MINUS
    call print_char
    pop rdi
    neg rdi
.print:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor  rax, rax
.loop:
    mov r8B, byte [rdi+rax]  
    cmp r8B, byte [rsi+rax]
    jne   .is_not_equals
    cmp byte[rdi+rax], 0
    je    .is_equals
    inc  rax
    jmp .loop 
.is_equals:
    mov rax, 1
    ret
.is_not_equals:
    mov rax, 0
    ret                            

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYS_READ
    mov rdi, FD_STDIN
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret
    
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r13
    push r12
    push r14
    xor r14, r14
    mov r12, rdi
    mov r13, rsi
    dec r13
.spaceLoop:
    call read_char
    cmp rax, SPACE
    je .spaceLoop
    cmp rax, TAB
    je .spaceLoop
    cmp rax, NEW_LINE
    je .spaceLoop
.loop:
    cmp rax, 0 
    je .read_success
    cmp rax, SPACE
    je .read_success
    cmp rax, TAB
    je .read_success
    cmp rax, NEW_LINE
    je .read_success

    cmp r13, r14
    je .read_failed
    mov [r12+r14], al
    inc r14
    call read_char 
    jmp .loop 
.read_success:
    mov byte[r12+r14], 0
    mov rdx, r14
    mov rax, r12
    jmp .return
.read_failed:
    xor rax, rax
.return:
    pop r14
    pop r12
    pop r13
    ret
    
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r12
    push r13
    push r14
    mov r13, 10
    xor r14, r14
    xor r12, r12
    xor rax, rax
    mov r14b, byte[rdi]
    cmp r14b, ZERO_DIGIT
    jb .parse_failed
    cmp r14b, NINE_DIGIT
    ja .parse_failed
.loop:
    sub r14b, ZERO_DIGIT
    add rax, r14
    inc r12
    mov r14b, byte[rdi+r12]
    cmp r14b, ZERO_DIGIT
    jb .parse_success
    cmp r14b, NINE_DIGIT
    ja .parse_success
    mul r13
    jmp .loop
.parse_failed:
    xor rdx, rdx
    jmp .return
.parse_success:
    mov rdx, r12
.return:   
    pop r14
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov sil, byte[rdi]
    cmp sil, MINUS
    je .minus
    cmp sil, PLUS
    je .plus
    cmp sil, ZERO_DIGIT
    jb .parse_failed
    cmp sil, NINE_DIGIT
    ja .parse_failed
    jmp parse_uint
.minus:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
.plus:
    inc rdi
    call parse_uint
    inc rdx
    ret
.parse_failed:
    xor rdx, rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r8, r8
    xor rax, rax
    xor rcx, rcx
.loop:
    mov cl, byte[rdi+r8]
    cmp rdx, r8
    je .return
    mov byte[rsi+r8], cl
    cmp cl, 0
    je .success
    inc r8
    jmp .loop
.success:
    mov rax, r8
.return:
    ret

print_error:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax 
    mov  rax, SYS_WRITE
    mov  rdi, FD_STDERR
    syscall
    ret
